# https://docs.gitlab.com/ee/ci/yaml/README.html#workflowrules-templates
include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - component: ${CI_SERVER_FQDN}/components/secret-detection/secret-detection@1.1.2
    inputs:
      stage: scan
  - component: $CI_SERVER_FQDN/components/dependency-scanning/main@0
    inputs:
      stage: scan
  - component: ${CI_SERVER_FQDN}/components/container-scanning/container-scanning@4.2.0
    inputs:
      stage: scan
      cs_image: $OCI_REGISTRY/$AGENTK_IMAGE_REPOSITORY:$AGENTK_IMAGE_TAG_FOR_CI
  - component: ${CI_SERVER_FQDN}/gitlab-org/components/danger-review/danger-review@2.0.0
    inputs:
      job_allow_failure: true

default:
  tags:
    - gitlab-org

variables:
  # Runner feature flags
  # https://docs.gitlab.com/runner/configuration/feature-flags.html#available-feature-flags
  # The image we use runs as user "ci" but by default code is cloned as "root".
  # https://gitlab.com/gitlab-org/gitlab/-/issues/444658
  FF_DISABLE_UMASK_FOR_DOCKER_EXECUTOR: "true"

  # CI image for all KAS and agentk builds
  # Image built using https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent-ci-image
  # and pushed into https://hub.docker.com/repository/docker/gitlab/gitlab-agent-ci-image
  BUILD_IMAGE_NAME: "gitlab/gitlab-agent-ci-image"
  # must use image digest to invalidate cache if image is updated.
  BUILD_IMAGE_SHA: "latest@sha256:4eb50ff684a62e4569561fa745db006255d5a2d13efc7b06c85073e6900dd358"
  FIPS_BUILD_IMAGE_SHA: "latest-fips@sha256:0169f709a02c20597f8cd85e614ced72799fa7169b5a2b8981c9eb0ca2479fc2"

  # DIND configuration
  DOCKER_VERSION: "28.0.1" # https://hub.docker.com/_/docker
  DOCKER_TLS_CERTDIR: "/certs"

  # Ruby version to use for the `gitlab-kas-grpc` Gem build, tests and deploy.
  RUBY_VERSION: '3.2'

  # Agentk image properties
  OCI_REGISTRY: $CI_REGISTRY_IMAGE
  AGENTK_IMAGE_REPOSITORY: agentk
  AGENTK_FIPS_IMAGE_REPOSITORY: agentk-fips
  AGENTK_IMAGE_TAG_FOR_CI: $CI_COMMIT_SHA
  AGENTK_IMAGE_REF: $OCI_REGISTRY/$AGENTK_IMAGE_REPOSITORY:$AGENTK_IMAGE_TAG_FOR_CI
  AGENTK_FIPS_IMAGE_REF: $OCI_REGISTRY/$AGENTK_FIPS_IMAGE_REPOSITORY:$AGENTK_IMAGE_TAG_FOR_CI

stages:
  - lint
  - build
  - test
  - scan
  - deploy
  - release

.use-ci-image:
  image:
    name: "$BUILD_IMAGE_NAME:$BUILD_IMAGE_SHA"
    entrypoint: [""]
    docker:
      user: "ci"

.configure-registry-access: &configure_registry_access
  - mkdir -p "$HOME/.docker"
  - |
    credentials=$(echo -n "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" | base64 -w0)
    cat << EOF > "$HOME/.docker/config.json"
    {
      "auths": {
          "$CI_REGISTRY": {
            "auth": "$credentials"
          }
      }
    }
    EOF

.bazel:base:
  extends:
    - .use-ci-image
  before_script:
    - echo "common --verbose_failures" >> .bazelrc
    - echo "common --curses=no" >> .bazelrc
    - echo "common --show_timestamps" >> .bazelrc
    - echo "common --http_timeout_scaling=3.0" >> .bazelrc
    - |
      if [[ -f "$GCP_REMOTE_CACHE_BUCKET_GOOGLE_APPLICATION_CREDENTIALS" ]]; then
        export GOOGLE_APPLICATION_CREDENTIALS="$GCP_REMOTE_CACHE_BUCKET_GOOGLE_APPLICATION_CREDENTIALS"
        echo "common --google_default_credentials" >> .bazelrc
        echo "common --remote_cache=$GCP_REMOTE_CACHE_BUCKET_URL/$(cat .bazelversion)-$BUILD_IMAGE_SHA" >> .bazelrc
      fi
    - echo "common --test_output=all" >> .bazelrc
    - echo "common --test_arg=-test.v" >> .bazelrc
    # - echo "common --sandbox_base=/dev/shm" >> .bazelrc # disabled because it's not big enough

.fips:base:
  image:
    name: "${BUILD_IMAGE_NAME}:${FIPS_BUILD_IMAGE_SHA}"
    entrypoint: [""]
    docker:
      user: "ci"
  services:
    - "docker:${DOCKER_VERSION}-dind"
  variables:
    BUILDER_IMAGE: "${BUILD_IMAGE_NAME}:${FIPS_BUILD_IMAGE_SHA}"
    # Docker vars are documented here https://docs.docker.com/engine/reference/commandline/cli/
    # Vars below mimic logic from https://github.com/docker-library/docker/blob/master/20.10/cli/docker-entrypoint.sh
    # Daemon logic: https://github.com/docker-library/docker/blob/master/20.10/dind/dockerd-entrypoint.sh
    DOCKER_HOST: "tcp://docker:2376"
    DOCKER_TLS_VERIFY: "1"
    DOCKER_CERT_PATH: "${DOCKER_TLS_CERTDIR}/client"
  before_script:
    - *configure_registry_access

.fips:platform-matrix:
  parallel:
    matrix:
      - ARCH: arm64
        RUNNER_TAG: saas-linux-medium-arm64
      - ARCH: amd64
        RUNNER_TAG: saas-linux-large-amd64
  tags:
    - "${RUNNER_TAG}"

# Job template to support image signing
# see https://docs.gitlab.com/ee/ci/yaml/signing_examples.html
.cosign:setup:
  variables:
    COSIGN_YES: "true"  # Used by Cosign to skip confirmation prompts for non-destructive operations
  id_tokens:
    SIGSTORE_ID_TOKEN:
      aud: sigstore

# Job that makes sure that when running a pipeline on a version tag (for a release)
# the version in that tag matches the contents of the `VERSION` file.
sanity-check-version-file:
  stage: .pre
  rules:
    - if: '$CI_COMMIT_TAG =~ /v.*/'
  image: alpine:3.19
  script:
    - version_from_tag=$(echo "$CI_COMMIT_TAG" | sed 's/^v//')
    - version_from_file=$(cat ./VERSION)
    - |
      if [ "$version_from_tag" = "$version_from_file" ]; then
          echo "Version in file matches version in Git tag"
      else
          echo "Version in file ('$version_from_file') doesn't match version in Git tag ('$version_from_tag')"
          exit 1
      fi

golangci:
  stage: lint
  needs: []
  image: golangci/golangci-lint:v1.64.5-alpine
  tags:
    - saas-linux-large-amd64
  script:
    # Write the code coverage report to gl-code-quality-report.json
    # and print linting issues to stdout in the format: path/to/file:line description
    - apk --no-cache add jq
    - golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json

verify:
  extends:
    - .bazel:base
  stage: lint
  needs: []
  tags:
    - saas-linux-large-amd64
  script:
    - make verify-ci
    - go mod graph > go.graph
  artifacts:
    when: on_success
    access: developer
    paths: ["go.graph"]

verify:temporal:determinism:
  extends:
    - .use-ci-image
  stage: lint
  before_script: go install go.temporal.io/sdk/contrib/tools/workflowcheck@latest
  script: $(go env GOPATH)/bin/workflowcheck ./...

unit-test:
  extends:
    - .bazel:base
    - .cosign:setup
  stage: test
  needs: []
  services:
    - redis
  tags:
    - saas-linux-large-amd64
  script:
    - echo 'common --test_env=REDIS_URL=redis://redis:6379' >> .bazelrc
    - make test-ci

unit-test:fips:
  extends:
    - .fips:base
    - .fips:platform-matrix
  stage: test
  needs: []
  services:
    - "docker:${DOCKER_VERSION}-dind"
    - redis
  variables:
    REDIS_URL: 'redis://redis:6379'
  script:
    - make test-ci-fips

# FAILS IN CI, SO DISABLED FOR NOW
#integration-test:
#  extends:
#    - .use-ci-image
#  stage: test
#  services:
#    - "docker:${DOCKER_VERSION}-dind"
#  tags:
#    - saas-linux-large-amd64
#  variables:
#    # Docker vars are documented here https://docs.docker.com/engine/reference/commandline/cli/
#    # Vars below mimic logic from https://github.com/docker-library/docker/blob/master/20.10/cli/docker-entrypoint.sh
#    # Daemon logic: https://github.com/docker-library/docker/blob/master/20.10/dind/dockerd-entrypoint.sh
#    DOCKER_HOST: "tcp://docker:2376"
#    DOCKER_TLS_VERIFY: "1"
#    DOCKER_CERT_PATH: "${DOCKER_TLS_CERTDIR}/client"
#    RACE_MODE: "1"
#  script:
#    - make test-it

agentk:image:ci:
  extends:
    - .bazel:base
    - .cosign:setup
  stage: build
  needs: []
  tags:
    - saas-linux-large-amd64
  script:
    - *configure_registry_access
    - make agentk-image-build-push-ci

agentk:fips-image:ci:
  extends:
    - .fips:base
    - .fips:platform-matrix
    - .cosign:setup
  stage: build
  needs: []
  script:
    - make agentk-fips-image-build-push-ci

agentk:fips-image:manifest:ci:
  extends:
    - .fips:base
  stage: build
  needs: ['agentk:fips-image:ci']
  script:
    - make agentk-fips-image-manifest-build-push-ci

gem:build:
  stage: build
  needs: []
  image: ruby:${RUBY_VERSION}
  script: make build-gem
  artifacts:
    paths:
      - pkg/ruby/gitlab-kas-grpc-*.gem

gem:smoke-test:
  image: ruby:${RUBY_VERSION}
  stage: test
  needs: ['gem:build']
  before_script: gem install pkg/ruby/gitlab-kas-grpc-*.gem
  script: echo 'Gitlab::Agent' | irb -r 'gitlab-kas-grpc'

agentk:image:tag:
  extends:
    - .bazel:base
    - .cosign:setup
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
  tags:
    - saas-linux-medium-amd64
  script:
    - *configure_registry_access
    - make release-tag

# Deprecated: will be removed in 18.0
# see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/655
deprecated:agentk:image:tag:stable:
  extends:
    - .bazel:base
    - .cosign:setup
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
  tags:
    - saas-linux-medium-amd64
  script:
    - git fetch
    - |
      if ! [[ $(git branch --remotes --contains "tags/${CI_COMMIT_TAG}" | grep "${CI_DEFAULT_BRANCH}") ]]; then
        echo "Tag wasn't created from a commit shared with the default branch. We don't tag as stable."
        exit 0
      fi
    - *configure_registry_access
    - make release-stable

agentk:fips-image:tag:
  extends:
    - .fips:base
    - .fips:platform-matrix
    - .cosign:setup
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - make release-tag-fips

agentk:fips-image:tag:manifest:
  extends:
    - .fips:base
  stage: deploy
  needs: ["agentk:fips-image:tag"]
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - make release-tag-fips-manifest

gem:publish:
  stage: deploy
  rules:
    - if: '$CI_SERVER_HOST == "gitlab.com" && $CI_PROJECT_NAMESPACE == "gitlab-org/cluster-integration" && $CI_COMMIT_TAG'
  image: ruby:${RUBY_VERSION}
  script: make publish-gem

release:
  stage: release
  rules:
    - if: $CI_COMMIT_TAG
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo 'release job'
  release:
    name: "$CI_COMMIT_TAG"
    tag_name: "$CI_COMMIT_TAG"
    ref: "$CI_COMMIT_TAG"
    description: "Release for tag $CI_COMMIT_TAG"

dependency-scanning:
  needs: [verify]

container_scanning:
  # NOTE: we cannot configure it via the component inputs.
  needs:
    - agentk:image:ci

sast:
  stage: scan
  needs: []
  variables:
    # We already run gosec as part of the lint job, no point in running it again here.
    SAST_DEFAULT_ANALYZERS: kubesec
    SCAN_KUBERNETES_MANIFESTS: "true"

# enable container scanning with https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners
appsec_container_scanning:
  stage: scan
  image: registry.gitlab.com/gitlab-com/gl-security/appsec/container-scanners:latest
  environment:
    name: appsec_container_scanning
    action: prepare
  tags:
    - saas-linux-medium-amd64
  script:
    - images="$AGENTK_IMAGE_REF,$AGENTK_FIPS_IMAGE_REF"
    - echo "Scanning $images"
    - /run/appsec-container-scan "$images" > gl-container-scanning-report.json
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
  rules:
    # Skip on forks, because external contributors can't run this pipeline
    - if: $CONTAINER_SCAN_PIPELINE_TRIGGER_TOKEN
      allow_failure: true
  needs:
    - agentk:image:ci
    - agentk:fips-image:manifest:ci

build-package-and-qa:
  stage: build
  trigger:
    project: 'gitlab-org/build/omnibus-gitlab-mirror'
    branch: 'master'
  inherit:
    variables: false
  variables:
    GITLAB_KAS_VERSION: $CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
    TOP_UPSTREAM_SOURCE_PROJECT: $CI_PROJECT_PATH
    TOP_UPSTREAM_SOURCE_REF: $CI_COMMIT_REF_NAME
    TOP_UPSTREAM_SOURCE_JOB: $CI_JOB_URL
    ee: "true"
  rules:
    # For MRs that change dependencies, we want to automatically ensure builds
    # aren't broken. In such cases, we don't want the QA tests to be run
    # automatically, but still available for developers to manually run.
    - if: $CI_MERGE_REQUEST_IID
      changes:
        - go.sum
      variables:
        BUILD_ON_ALL_OS: "true"
        MANUAL_QA_TEST: "true"
      allow_failure: false
    # For other MRs, we still provide this job as a manual job for developers
    # to obtain a package for testing and run QA tests.
    - if: $CI_MERGE_REQUEST_IID
      when: manual
      allow_failure: true
  needs: []
