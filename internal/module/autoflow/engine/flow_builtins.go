package engine

import (
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow/flow"
)

var (
	_ flow.Builtins = (*flowBuiltins)(nil)
)

type flowBuiltins struct {
	printFunc func(msg string)
	nowFunc   func() time.Time
}

func (b *flowBuiltins) Print(msg string) {
	b.printFunc(msg)
}

func (b *flowBuiltins) Now() time.Time {
	return b.nowFunc()
}
