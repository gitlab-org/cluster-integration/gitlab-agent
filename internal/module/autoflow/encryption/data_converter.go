package encryption

import (
	"go.temporal.io/sdk/converter"
)

type DataConverterOptions struct {
	// secretKey secret to use for AES-GCM encryption and decryption.
	SecretKey []byte
}

func NewDataConverter(wrappedDataConverter converter.DataConverter, opts DataConverterOptions) converter.DataConverter {
	return converter.NewCodecDataConverter(
		wrappedDataConverter,
		NewCodec(opts.SecretKey),
	)
}
