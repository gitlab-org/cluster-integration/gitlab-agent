package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/event_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
)

type Factory struct {
	EventTracker event_tracker.EventTrackerCollector
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	return newModule(config, f.EventTracker)
}

func (f *Factory) Name() string {
	return event_tracker.ModuleName
}
