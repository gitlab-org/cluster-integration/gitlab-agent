package server

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testlogger"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/version"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/entity"
	"go.uber.org/mock/gomock"
)

var (
	_ modserver.Factory = (*Factory)(nil)
)

func TestServer_GetConnectedAgentsByProjectIDs(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	agentID1 := testhelpers.AgentID
	agentID2 := testhelpers.AgentID + 1
	projectID1 := testhelpers.ProjectID
	projectID2 := testhelpers.ProjectID + 1
	req := &rpc.GetConnectedAgentsByProjectIDsRequest{
		ProjectIds: []int64{projectID1, projectID2},
	}

	mockRPCAPI.EXPECT().
		Log().
		Return(testlogger.New(t))

	mockTracker.EXPECT().
		GetConnectionsByProjectID(ctx, projectID1, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      agentID1,
				ProjectId:    projectID1,
			})
			require.NoError(t, err)
			return err
		})
	mockTracker.EXPECT().
		GetConnectionsByProjectID(ctx, projectID2, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      agentID2,
				ProjectId:    projectID2,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByProjectIDs(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, agentID1, resp.Agents[0].AgentId)
	assert.EqualValues(t, agentID2, resp.Agents[1].AgentId)
}

func TestServer_GetConnectedAgentsByProjectIDs_WithAgentWarning(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	agentID1 := testhelpers.AgentID
	agentID2 := testhelpers.AgentID + 1
	projectID1 := testhelpers.ProjectID
	projectID2 := testhelpers.ProjectID + 1
	req := &rpc.GetConnectedAgentsByProjectIDsRequest{
		ProjectIds: []int64{projectID1, projectID2},
	}

	mockRPCAPI.EXPECT().
		Log().
		Return(testlogger.New(t))

	mockTracker.EXPECT().
		GetConnectionsByProjectID(ctx, projectID1, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					// This is a major version higher than the server
					Version: "13.0.0",
				},
				ConnectionId: 123123123,
				AgentId:      agentID1,
				ProjectId:    projectID1,
			})
			require.NoError(t, err)
			return err
		})
	mockTracker.EXPECT().
		GetConnectionsByProjectID(ctx, projectID2, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      agentID2,
				ProjectId:    projectID2,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByProjectIDs(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, agentID1, resp.Agents[0].AgentId)
	assert.Equal(t, "The agent server for Kubernetes (KAS) version cannot be checked for compatibility. KAS and the agent for Kubernetes (agentk) might not be compatible. Make sure agentk and KAS have the same version.", resp.Agents[0].Warnings[0].GetVersion().Message)
	assert.EqualValues(t, agentID2, resp.Agents[1].AgentId)
}

func TestServer_GetConnectedAgentsByAgentIDs(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	agentID1 := testhelpers.AgentID
	agentID2 := testhelpers.AgentID + 1
	projectID1 := testhelpers.ProjectID
	projectID2 := testhelpers.ProjectID + 1
	req := &rpc.GetConnectedAgentsByAgentIDsRequest{
		AgentIds: []int64{agentID1, agentID2},
	}

	mockRPCAPI.EXPECT().
		Log().
		Return(testlogger.New(t))

	mockTracker.EXPECT().
		GetConnectionsByAgentID(ctx, agentID1, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      agentID1,
				ProjectId:    projectID1,
			})
			require.NoError(t, err)
			return err
		})
	mockTracker.EXPECT().
		GetConnectionsByAgentID(ctx, agentID2, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      agentID2,
				ProjectId:    projectID2,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByAgentIDs(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, agentID1, resp.Agents[0].AgentId)
	assert.EqualValues(t, agentID2, resp.Agents[1].AgentId)
}

func TestServer_GetConnectedAgentsByAgentIDs_WithAgentWarning(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	agentID1 := testhelpers.AgentID
	agentID2 := testhelpers.AgentID + 1
	projectID1 := testhelpers.ProjectID
	projectID2 := testhelpers.ProjectID + 1
	req := &rpc.GetConnectedAgentsByAgentIDsRequest{
		AgentIds: []int64{agentID1, agentID2},
	}

	mockRPCAPI.EXPECT().
		Log().
		Return(testlogger.New(t))

	mockTracker.EXPECT().
		GetConnectionsByAgentID(ctx, agentID1, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				AgentMeta: &entity.AgentMeta{
					// This is a major version higher than the server
					Version: "13.0.0",
				},
				ConnectionId: 123123123,
				AgentId:      agentID1,
				ProjectId:    projectID1,
			})
			require.NoError(t, err)
			return err
		})
	mockTracker.EXPECT().
		GetConnectionsByAgentID(ctx, agentID2, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      agentID2,
				ProjectId:    projectID2,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByAgentIDs(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, agentID1, resp.Agents[0].AgentId)
	assert.Equal(t, "The agent server for Kubernetes (KAS) version cannot be checked for compatibility. KAS and the agent for Kubernetes (agentk) might not be compatible. Make sure agentk and KAS have the same version.", resp.Agents[0].Warnings[0].GetVersion().Message)
	assert.EqualValues(t, agentID2, resp.Agents[1].AgentId)
}

func TestServer_CountAgentsByAgentVersions(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	req := &rpc.CountAgentsByAgentVersionsRequest{}

	mockRPCAPI.EXPECT().
		Log().
		Return(testlogger.New(t))

	mockTracker.EXPECT().
		CountAgentsByAgentVersions(ctx).
		DoAndReturn(func(ctx context.Context) (map[string]int64, error) {
			counts := map[string]int64{
				"16.8.0": 111,
				"16.9.0": 222,
			}
			return counts, nil
		})

	resp, err := s.CountAgentsByAgentVersions(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.AgentVersions, 2)
	assert.EqualValues(t, 111, resp.AgentVersions["16.8.0"])
	assert.EqualValues(t, 222, resp.AgentVersions["16.9.0"])
}

func setupServer(t *testing.T) (*mock_modserver.MockAgentRPCAPI, *mock_agent_tracker.MockTracker, *server, context.Context) {
	ctrl := gomock.NewController(t)

	mockRPCAPI := mock_modserver.NewMockAgentRPCAPI(ctrl)
	mockTracker := mock_agent_tracker.NewMockTracker(ctrl)

	s := &server{
		agentQuerier:       mockTracker,
		serverVersion:      version.Version{Major: 12, Minor: 3, Patch: 4},
		gitlabReleasesList: []string{"12.3.4"},
	}

	ctx := modshared.InjectRPCAPI(context.Background(), mockRPCAPI)

	return mockRPCAPI, mockTracker, s, ctx
}
