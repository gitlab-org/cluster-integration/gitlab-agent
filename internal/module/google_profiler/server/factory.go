package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/google_profiler"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
)

const (
	// 6 chars is the minimum that can be used to search for a commit in GoLand.
	gitRefChars = 6
)

type Factory struct {
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	if !config.Config.Observability.GoogleProfiler.Enabled {
		return nil, nil
	}
	return &module{
		cfg:     config.Config.Observability.GoogleProfiler,
		service: config.KASName,
		version: constructVersion(config),
	}, nil
}

func (f *Factory) Name() string {
	return google_profiler.ModuleName
}

func constructVersion(config *modserver.Config) string {
	return config.Version.String() + "+" + config.GitRef[:min(gitRefChars, len(config.GitRef))]
}
