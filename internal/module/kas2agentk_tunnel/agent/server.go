package agent

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kas2agentk_tunnel/router"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
	"google.golang.org/grpc"
)

type server struct {
	rpc.UnsafeReverseTunnelServer
	registry *router.Registry
}

func (s *server) Connect(server grpc.BidiStreamingServer[rpc.ConnectRequest, rpc.ConnectResponse]) error {
	ctx := server.Context()
	ageCtx := grpctool.MaxConnectionAgeContextFromStreamContext(ctx)
	return s.registry.HandleTunnel(ageCtx, server)
}
