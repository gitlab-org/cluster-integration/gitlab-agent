package server

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"google.golang.org/grpc/metadata"
)

func TestPrepareMetadata_HandlesNilMD(t *testing.T) {
	md := prepareMetadata(testhelpers.AgentID, nil)
	assert.Equal(t, metadata.MD{agentIDMDKey: []string{"123"}}, md)
}

func TestPrepareMetadata_HandlesEmptyMD(t *testing.T) {
	md := prepareMetadata(testhelpers.AgentID, metadata.MD{})
	assert.Equal(t, metadata.MD{agentIDMDKey: []string{"123"}}, md)
}

func TestPrepareMetadata_HappyPath(t *testing.T) {
	md := prepareMetadata(testhelpers.AgentID, metadata.MD{"some-key": []string{"abc"}})
	assert.Equal(t, metadata.MD{
		"some-key":   []string{"abc"},
		agentIDMDKey: []string{"123"},
	}, md)
}

func TestPrepareMetadata_OverwritesRougeKey(t *testing.T) {
	md := prepareMetadata(testhelpers.AgentID, metadata.MD{agentIDMDKey: []string{"333"}})
	assert.Equal(t, metadata.MD{
		agentIDMDKey: []string{"123"},
	}, md)
}
