package agent

import (
	"net/url"

	"github.com/bufbuild/protovalidate-go"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/kubernetes_api/rpc"
	"k8s.io/client-go/rest"
)

type server struct {
	rpc.UnsafeKubernetesApiServer
	validator  protovalidate.Validator
	restConfig *rest.Config
	baseURL    *url.URL
	userAgent  string
	version    string
	via        string
}

func newServer(v protovalidate.Validator, restConfig *rest.Config, baseURL *url.URL, userAgent, version string) *server {
	return &server{
		validator:  v,
		restConfig: restConfig,
		baseURL:    baseURL,
		userAgent:  userAgent,
		version:    version,
		via:        "gRPC/1.0 " + userAgent,
	}
}
