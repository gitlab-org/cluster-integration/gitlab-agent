package rpc

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func (x *Object) GroupVersionKind() schema.GroupVersionKind {
	return schema.GroupVersionKind{
		Group:   x.Group,
		Version: x.Version,
		Kind:    x.Kind,
	}
}

func (x *Object) GroupKind() schema.GroupKind {
	return schema.GroupKind{
		Group: x.Group,
		Kind:  x.Kind,
	}
}
