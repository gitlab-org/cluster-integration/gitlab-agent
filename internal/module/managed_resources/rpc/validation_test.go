package rpc

import (
	"testing"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
)

func TestValidation_Valid(t *testing.T) {
	tests := []testhelpers.ValidTestcase{
		{
			Name: "EnvironmentTemplateInfo",
			Valid: &EnvironmentTemplateInfo{
				AgentName:    "abc",
				TemplateName: "abc",
			},
		},
	}
	testhelpers.AssertValid(t, tests)
}

func TestValidation_Invalid(t *testing.T) {
	tests := []testhelpers.InvalidTestcase{
		{
			ErrString: "validation error:\n - agent_name: invalid agent name [string.is_agent_name]\n - template_name: value length must be at least 1 bytes [string.min_bytes]",
			Invalid:   &EnvironmentTemplateInfo{},
		},
	}
	testhelpers.AssertInvalid(t, tests)
}
