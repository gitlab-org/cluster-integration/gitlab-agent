package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
)

const (
	defaultCodecServerListenNetwork = "tcp"
	defaultCodecServerListenAddress = ":8142"
)

func ApplyDefaults(config *kascfg.ConfigurationFile) {
	if config.Autoflow == nil { // module disabled. This is temporary.
		return
	}

	prototool.NotNil(&config.Autoflow.Temporal.WorkflowDataEncryption)
	prototool.NotNil(&config.Autoflow.Temporal.WorkflowDataEncryption.CodecServer)
	prototool.NotNil(&config.Autoflow.Temporal.WorkflowDataEncryption.CodecServer.Listen)
	prototool.DefaultValPtr(&config.Autoflow.Temporal.WorkflowDataEncryption.CodecServer.Listen.Network, defaultCodecServerListenNetwork)
	prototool.DefaultVal(&config.Autoflow.Temporal.WorkflowDataEncryption.CodecServer.Listen.Address, defaultCodecServerListenAddress)
}
