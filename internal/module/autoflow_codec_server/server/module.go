package server

import (
	"context"
	"log/slog"
	"net"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/autoflow_codec_server"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
)

type module struct {
	log      *slog.Logger
	server   server
	listener func() (net.Listener, error)
}

func (m *module) Run(ctx context.Context) error {
	lis, err := m.listener()
	if err != nil {
		return err
	}
	// Error is ignored because server.Run() closes the listener and
	// a second close always produces an error.
	defer lis.Close() //nolint:errcheck

	m.log.Info("AutoFlow codec server for Temporal Web UI is up",
		logz.NetNetworkFromAddr(lis.Addr()),
		logz.NetAddressFromAddr(lis.Addr()),
	)
	return m.server.Run(ctx, lis)
}

func (m *module) Name() string {
	return autoflow_codec_server.ModuleName
}
