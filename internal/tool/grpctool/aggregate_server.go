package grpctool

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	_ GRPCServer = AggregateServer{}
)

type GRPCServer interface {
	grpc.ServiceRegistrar
	reflection.ServiceInfoProvider
}

type AggregateServer []GRPCServer

func (s AggregateServer) RegisterService(desc *grpc.ServiceDesc, impl any) {
	for _, server := range s {
		server.RegisterService(desc, impl)
	}
}

func (s AggregateServer) GetServiceInfo() map[string]grpc.ServiceInfo {
	if len(s) > 0 {
		return s[0].GetServiceInfo()
	}
	return nil
}
