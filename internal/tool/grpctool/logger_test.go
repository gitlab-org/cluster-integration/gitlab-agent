package grpctool

import (
	"bytes"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
)

func TestLogger(t *testing.T) {
	b := &bytes.Buffer{}
	l := Logger{
		Handler: slog.NewTextHandler(b, &slog.HandlerOptions{
			AddSource: true,
			Level:     slog.LevelDebug,
			ReplaceAttr: func(groups []string, attr slog.Attr) slog.Attr {
				if attr.Key == slog.TimeKey {
					return slog.Attr{}
				}
				return logz.TrimSourceFilePath(attr)
			},
		}),
	}

	l.Info(1, 2)
	l.Infoln(1, 2)
	l.Infof("%d %d", 1, 2)
	l.InfoDepth(0, 1, 2)
	l.Warning(1, 2)
	l.Warningln(1, 2)
	l.Warningf("%d %d", 1, 2)
	l.WarningDepth(0, 1, 2)
	l.Error(1, 2)
	l.Errorln(1, 2)
	l.Errorf("%d %d", 1, 2)
	l.ErrorDepth(0, 1, 2)

	expected := `level=INFO source=logger_test.go:27 msg="1 2"
level=INFO source=logger_test.go:28 msg="1 2"
level=INFO source=logger_test.go:29 msg="1 2"
level=INFO source=logger_test.go:30 msg="1 2"
level=WARN source=logger_test.go:31 msg="1 2"
level=WARN source=logger_test.go:32 msg="1 2"
level=WARN source=logger_test.go:33 msg="1 2"
level=WARN source=logger_test.go:34 msg="1 2"
level=ERROR source=logger_test.go:35 msg="1 2"
level=ERROR source=logger_test.go:36 msg="1 2"
level=ERROR source=logger_test.go:37 msg="1 2"
level=ERROR source=logger_test.go:38 msg="1 2"
`

	assert.Equal(t, expected, b.String())
}
