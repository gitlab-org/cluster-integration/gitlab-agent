package prototool

import "google.golang.org/protobuf/proto"

type NopValidator struct{}

func (v NopValidator) Validate(proto.Message) error {
	return nil
}
