load("@io_bazel_rules_go//go:def.bzl", "go_library")
load("//build:build.bzl", "go_custom_test")
load("//build:proto.bzl", "go_proto_generate")

go_proto_generate(
    src = "api.proto",
    deps = [
        "//pkg/agentcfg:proto",
        "//pkg/constraint:proto",
        "//pkg/entity:proto",
        "@protovalidate//:proto",
    ],
)

go_library(
    name = "api",
    srcs = [
        "api.pb.go",
        "api_extra.go",
        "authorize_proxy_user.go",
        "get_agent_info.go",
        "get_allowed_agents.go",
        "get_project_info.go",
        "get_receptive_agents.go",
        "get_repository_info.go",
        "helper.go",
        "module_request.go",
        "post_agent_configuration.go",
        "send_event.go",
        "send_usage_ping.go",
        "verify_project_access.go",
    ],
    importpath = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab/api",
    visibility = ["//:__subpackages__"],
    deps = [
        "//internal/api",
        "//internal/gitlab",
        "//pkg/agentcfg",
        "//pkg/constraint",
        "//pkg/entity",
        "@build_buf_gen_go_bufbuild_protovalidate_protocolbuffers_go//buf/validate",
        "@org_golang_google_protobuf//reflect/protoreflect",
        "@org_golang_google_protobuf//runtime/protoimpl",
    ],
)

go_custom_test(
    name = "api_test",
    srcs = [
        "authorize_proxy_user_test.go",
        "get_agent_info_test.go",
        "get_project_info_test.go",
        "helper_test.go",
        "post_agent_configuration_test.go",
        "validation_test.go",
        "verify_project_access_test.go",
    ],
    embed = [":api"],
    deps = [
        "//internal/gitaly/vendored/gitalypb",
        "//internal/tool/testing/matcher",
        "//internal/tool/testing/mock_gitlab",
        "//internal/tool/testing/testhelpers",
        "//pkg/agentcfg",
        "//pkg/entity",
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//require",
        "@org_golang_google_protobuf//encoding/protojson",
    ],
)
