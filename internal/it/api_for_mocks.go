package it

import (
	"context"
	"net/http"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitaly/vendored/gitalypb"
	"google.golang.org/grpc"
)

//go:generate mockgen.sh -source "api_for_mocks.go" -destination "mock_for_test.go" -package "it"

// GitalyAPI is a subset of gitalypb.CommitServiceServer.
// We cannot generate a mock for the real interface since it has a private method. So, have to have this crutch.
// See https://github.com/uber-go/mock/issues/64.
type GitalyAPI interface {
	GetTreeEntries(*gitalypb.GetTreeEntriesRequest, grpc.ServerStreamingServer[gitalypb.GetTreeEntriesResponse]) error
	LastCommitForPath(context.Context, *gitalypb.LastCommitForPathRequest) (*gitalypb.LastCommitForPathResponse, error)
	TreeEntry(*gitalypb.TreeEntryRequest, grpc.ServerStreamingServer[gitalypb.TreeEntryResponse]) error
}

type GitLabAPI interface {
	GetAgentInfo(http.ResponseWriter, *http.Request)
	PostAgentConfiguration(http.ResponseWriter, *http.Request)
	GetAllowedAgents(http.ResponseWriter, *http.Request)
	GetReceptiveAgents(http.ResponseWriter, *http.Request)
}
