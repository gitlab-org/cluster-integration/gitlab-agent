# Integration tests

Integration tests are using [test containers](https://golang.testcontainers.org) to build kas and agentk images.
Tests also start:
- [K3s](https://k3s.io/) container to provide a Kubernetes instance.
- Redis

kas is started as a container, agentk is deployed as a `Pod` into Kubernetes.

## Running

From command line:

```shell
make test-it
```

If test containers cannot find your Docker-compatible endpoint, set the `DOCKER_HOST` environment variable.

You can also run/debug the tests from you favourite IDE.
Note that debugger will be attached to the test process, not to kas or agentk processes.
It might be possible to attach a debugger to those too, but it's likely more complicated than setting
it all up with GDK.
