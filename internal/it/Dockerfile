# Normally TARGETPLATFORM is set by Docker, but testcontainers doesn't set it.
ARG TARGETPLATFORM
ARG TARGETARCH
ARG BIN_DIR=/usr/bin

# https://hub.docker.com/_/debian
FROM --platform=${TARGETPLATFORM} debian:12-slim AS builder

# Use the injected value. See https://docs.docker.com/reference/dockerfile/#understand-how-arg-and-from-interact
ARG TARGETARCH
ARG BIN_DIR
ARG GO_VERSION
ARG GO_URL="https://storage.googleapis.com/golang/go${GO_VERSION}.linux-${TARGETARCH}.tar.gz"
ARG BUILD_DIR=/tmp/build

# Install go
RUN DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        curl git make gcc libc6-dev ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir "${BUILD_DIR}" \
    && cd "${BUILD_DIR}" \
    && curl --retry 6 -sfo golang.tar.xz "${GO_URL}" \
    && tar -xf golang.tar.xz \
    && mv "${BUILD_DIR}/go" /usr/local/go \
    && ln -sf /usr/local/go/bin/go /usr/local/bin/ \
    && rm -rf "${BUILD_DIR}"

WORKDIR /tmp/src
COPY . ./
RUN RACE_MODE=1 TARGET_DIRECTORY=${BIN_DIR} make kas agentk

FROM --platform=${TARGETPLATFORM} gcr.io/distroless/base-debian12:nonroot-${TARGETARCH}

ARG BIN_DIR

COPY --from=builder ${BIN_DIR}/kas ${BIN_DIR}/agentk ${BIN_DIR}/

ENTRYPOINT ["/usr/bin/kas"]
