package agentkapp

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/ash2k/stager"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/syncz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/agentcfg"
)

var (
	_ syncz.Layer = (*factoriesLayer)(nil)
)

type factoriesLayer struct {
	log       *slog.Logger
	config    func(name string) *modagent.Config
	lr        *leaderRunner
	mr        *moduleRunner
	factories []modagent.Factory
}

func (l *factoriesLayer) ToStageFuncs() ([]stager.StageFunc, error) {
	modules, err := l.constructModules()
	if err != nil {
		return nil, err
	}
	return append(
		l.mr.RegisterModules(modules...),
		func(stage stager.Stage) {
			stage.GoWhenDone(func() error {
				for _, mod := range modules {
					l.log.Info("Stopping", logz.ModuleName(mod.Name()))
				}
				return nil
			})
		},
	), nil
}

// constructModules constructs modules.
//
// factory.New() must be called from the main goroutine because:
// - it may mutate a gRPC server (register an API) and that can only be done before Serve() is called on the server.
// - we want anything that can fail to fail early, before starting any modules.
// Do not move into stager.StageFunc() below.
func (l *factoriesLayer) constructModules() ([]modagent.Module, error) {
	modules := make([]modagent.Module, 0, len(l.factories))
	for _, factory := range l.factories {
		name := factory.Name()
		log := l.log.With(logz.ModuleName(name))
		module, err := factory.New(l.config(name))
		if err != nil {
			return nil, fmt.Errorf("%s: %w", name, err)
		}
		if module == nil {
			log.Debug("Module is not started, because the factory did not create it")
			continue
		}
		module = &loggingModuleWrapper{
			Module: module,
			log:    log,
		}
		if factory.IsProducingLeaderModules() {
			module = l.lr.WrapModule(module)
		}
		modules = append(modules, module)
	}
	return modules, nil
}

type loggingModuleWrapper struct {
	modagent.Module
	log *slog.Logger
}

func (w *loggingModuleWrapper) Run(ctx context.Context, cfg <-chan *agentcfg.AgentConfiguration) error {
	w.log.Info("Starting")
	err := w.Module.Run(ctx, cfg)
	if err != nil {
		// This is a weird log+return, but we want to log ASAP to aid debugging.
		w.log.Error("Stopped with error", logz.Error(err))
		return fmt.Errorf("%s: %w", w.Name(), err)
	}
	w.log.Info("Stopped")
	return nil
}
