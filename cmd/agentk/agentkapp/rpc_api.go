package agentkapp

import (
	"log/slog"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/fieldz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
)

var (
	_ modshared.RPCAPI = (*agentRPCAPI)(nil)
)

type agentRPCAPI struct {
	modshared.RPCAPIStub
}

func (a *agentRPCAPI) HandleProcessingError(log *slog.Logger, msg string, err error, fields ...fieldz.Field) {
	handleProcessingError(a.StreamCtx, log, msg, err, fields...)
}

func (a *agentRPCAPI) HandleIOError(log *slog.Logger, msg string, err error) error {
	// The problem is almost certainly with the client's connection.
	// Still log it on Debug.
	log.Debug(msg, logz.Error(err))
	return grpctool.HandleIOError(msg, err)
}
