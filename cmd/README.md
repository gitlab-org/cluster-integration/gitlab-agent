## Commands

Commands in this repository follow the
[OpenShift guidelines](https://github.com/openshift/origin/blob/master/docs/cli_hacking_guide.adoc)
with the following differences:

- `xxxOptions` structs are private. No need to export them.
- `xxxOptions` structs are called just `options` to avoid repeating the package name.
- Methods and fields on `options` are not exported.
- Options struct constructor is `newOptions` - unexported and does not repeat the package name.
- Error handling and logging/printing is done using `cmd.LogAndExitOnError()` in `cobra.Command#Run()`.
  Do not return the error since it will be logged using the default logger in that case, not the configured logger.
