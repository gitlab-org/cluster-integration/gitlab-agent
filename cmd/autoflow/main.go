package main

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/cmd/autoflow/autoflowapp"
)

func main() {
	cmd.Run(autoflowapp.NewCommand())
}
