syntax = "proto3";

// If you make any changes make sure you run: make regenerate-proto

package gitlab.agent.event;

option go_package = "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/event";

import "buf/validate/validate.proto";
import "google/protobuf/any.proto";
import "google/protobuf/timestamp.proto";

message GitPushEvent {
  Project project = 1 [(buf.validate.field).required = true];
}

message Project {
  // The numeric GitLab project id
  int64 id = 1 [(buf.validate.field).int64.gt = 0];
  // The full path to the GitLab project
  string full_path = 2 [(buf.validate.field).string.min_bytes = 1];
}

// Messages below were copied from
// https://github.com/cloudevents/spec/blob/c48be59daf8ef3a3661fbb1a1b532efeab279b98/cloudevents/formats/cloudevents.proto
// on 2023-10-14.
// There is a Go module with the generated Go code at
// https://github.com/cloudevents/sdk-go/tree/main/binding/format/protobuf/v2
// but it was generated with the old/v1 proto compiler and contains dependencies we don't want.
// After the file was vendored, we've made changes to the file-level directives to make it suit our needs.

/*
 Copyright 2021 The CloudEvents Authors
 SPDX-License-Identifier: Apache-2.0
 */

/**
 * CloudEvent Protobuf Format
 *
 * - Required context attributes are explicitly represented.
 * - Optional and Extension context attributes are carried in a map structure.
 * - Data may be represented as binary, text, or protobuf messages.
 */

message CloudEvent {

  // -- CloudEvent Context Attributes

  // Required Attributes
  string id = 1;
  string source = 2; // URI-reference
  string spec_version = 3;
  string type = 4;

  // Optional & Extension Attributes
  map<string, CloudEventAttributeValue> attributes = 5;

  // -- CloudEvent Data (Bytes, Text, or Proto)
  oneof  data {
    bytes binary_data = 6;
    string text_data = 7;
    google.protobuf.Any proto_data = 8;
  }

  /**
   * The CloudEvent specification defines
   * seven attribute value types...
   */

  message CloudEventAttributeValue {

    oneof attr {
      bool ce_boolean = 1;
      int32 ce_integer = 2;
      string ce_string = 3;
      bytes ce_bytes = 4;
      string ce_uri = 5;
      string ce_uri_ref = 6;
      google.protobuf.Timestamp ce_timestamp = 7;
    }
  }
}
